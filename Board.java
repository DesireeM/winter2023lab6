public class Board
{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board ()
	{
		die1 = new Die();
		die2 = new Die();
		
		tiles = new boolean[12];
	}
	
	public String toString()
	{
		String tilesStr = "";
		for (int i = 0; i < tiles.length ; i++)
		{
			if(tiles[i] == false)
			{
				tilesStr += (i + 1);
			}
			
			else
			{
				tilesStr += "X";
			}
		}
		return tilesStr;
	}
	
	public boolean playATurn() 
	{
		die1.roll();
		die2.roll();
		System.out.println(die1 + " " + die2);
		
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		System.out.println("Sum of dice: " + sumOfDice);
		
		if (tiles[sumOfDice - 1] == false)
		{
			tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			return false;
		}
		
		int firstDieValue = die1.getFaceValue();
		if (tiles[firstDieValue - 1] == false)
		{
			tiles[firstDieValue - 1] = true;
			System.out.println("Closing tile with the same value as die one: " + firstDieValue);
			return false;
		}
		
		int secondDieValue = die2.getFaceValue();
		if (tiles[secondDieValue - 1] == false)
		{
			tiles[secondDieValue - 1] = true;
			System.out.println("Closing tile with the same value as die one: " + secondDieValue);
			return false;
		}
		
		System.out.println("All the tiles for these values are already shut");
		return true;
	}
}