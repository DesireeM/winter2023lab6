public class Jackpot
{
	public static void main(String[] args) 
	{
		System.out.println("Welcome to my Jackpot game!");
		
        Board board = new Board();
        boolean gameOver = false;
        int numOfTilesClosed = 0;
		
		while(gameOver == false)
		{
			System.out.println(board);
			
			if (board.playATurn() == true)
			{
                gameOver = true;
            } 
			
			else 
			{
                numOfTilesClosed += 1;;
            }
		}
		
		if (numOfTilesClosed >= 7)
		{
			System.out.println("Congratulations! You won!");
        } 
		
		else 
		{
            System.out.println("You lost.");
        }
	}
}